import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.*;

public class Cliente extends Thread {
    public static boolean done = false;
    private Socket conexao;

    public Cliente(Socket s) {
        this.conexao = s;
    }

    public static void main(String[] args) {
        Socket conexao = new Socket(" localhost", 2000);
        PrintStream saida = new PrintStream(conexao.getOutputStream());

        BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Entre com o seu nome: ");
        String meuNome =  teclado.readLine();
        Thread t = new Cliente(conexao);
        t.start();
        while(true) {
            if(done) {
                break;
            }
            System.out.println("> ");
            linha = teclado.readLine();
            saida.println(linha);
        }
    }

    public void run() {
        BufferedReader entrada = new BufferedReader(new InputStreamReader(conexao.getInputStream()));
        String linha;
        while(true) {
            linha = entrada.readLine();
            if(linha.trim().equals("")) {
                System.out.println("Conexao encerrada!!!");
                break;
            }
            System.out.println();
            System.out.println(linha);
            System.out.print("....> ");
        }
        done = true;
    }
}
